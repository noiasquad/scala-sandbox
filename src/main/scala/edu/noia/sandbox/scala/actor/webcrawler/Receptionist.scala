package edu.noia.sandbox.scala.actor.webcrawler

import akka.actor.{Actor, ActorRef, Props}
import edu.noia.sandbox.scala.actor.webcrawler.Receptionist.{Failed, Job, Result}

class Receptionist extends Actor {
  override def receive: Receive = waiting

  /*
    upon Get(url) start a traversal and become running
   */
  val waiting: Receive = {
    case Get(url) => context.become(runNext(Vector(Job(sender, url))))
  }

  /*
    upon Get(url) append that to queue and keep running
    upon Controller.Result(links) ship that to client and run next job from queue if any
   */
  def running(queue: Vector[Job]): Receive = {
    case Controller.Result(links) =>
      val job = queue.head
      job.client ! Result(job.url, links)
      context.stop(sender)
      context.become(runNext(queue.tail))

    case Get(url) =>
      context.become(enqueueJob(queue, Job(sender, url)))
  }


  var requestNo = 0

  def runNext(queue: Vector[Job]): Receive = {
    requestNo += 1
    if (queue.isEmpty) waiting
    else {
      val controller = context.actorOf(Props[Controller], s"c$requestNo")
      controller ! Controller.Check(queue.head.url, 2)
      running(queue)
    }
  }

  def enqueueJob(queue: Vector[Job], job: Job): Receive = {
    if (queue.size > 3) {
      sender ! Failed(job.url)
      running(queue)
    } else running(queue :+ job)
  }

}

object Receptionist {

  case class Failed(url: String, comment: String = "")

  case class Result(url: String, links: Set[String])

  case class Job(client: ActorRef, url: String)

}
