package edu.noia.sandbox.scala.actor.webcrawler

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import akka.util.Timeout
import edu.noia.sandbox.scala.actor.webcrawler.Controller.{Check, Result}

import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global

class Controller extends Actor with ActorLogging {
  context.system.scheduler.scheduleOnce(5.seconds, self, Timeout)

  var cache = Set.empty[String]
  var children = Set.empty[ActorRef]

  override def receive: Receive = {
    case Check(url, depth) =>
      log.debug("{} checking {}", depth, url)

      if (!cache(url) && depth > 0)
        children += context.actorOf(Props(new Getter(url, depth - 1)))
      cache += url

    case Getter.Done =>
      children -= sender
      if (children.isEmpty) context.parent ! Result(cache)

    case Timeout =>
      log.debug("timeout of {} reached", 5.seconds)
      context.parent ! Result(cache)
  }
}

object Controller {

  case class Check(url: String, depth: Int)

  case class Result(urls: Set[String])

}
