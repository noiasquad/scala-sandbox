package edu.noia.sandbox.scala.actor.webcrawler

import akka.actor.{Actor, ActorRef}
import akka.pattern.pipe

class Cache extends Actor {
  implicit val exec = context.dispatcher

  var cache = Map.empty[String, String]

  override def receive: Receive = {
    case Get(url) =>
      if (cache contains url) sender ! cache(url)
      else {
        val client = sender
        AsyncWebClient get url map (Response(client, url, _)) pipeTo self
      }

    case Response(client, url, body) =>
      cache += (url -> body)
      client ! body
  }
}

case class Get(url: String)

private case class Response(client: ActorRef, url: String, body: String)
