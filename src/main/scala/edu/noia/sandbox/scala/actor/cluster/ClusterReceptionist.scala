package edu.noia.sandbox.scala.actor.cluster

import akka.actor.{Actor, Address, Props}
import akka.cluster.ClusterEvent.{MemberRemoved, MemberUp}
import akka.cluster.{Cluster, ClusterEvent}
import edu.noia.sandbox.scala.actor.webcrawler.Get
import edu.noia.sandbox.scala.actor.webcrawler.Receptionist.Failed

import scala.util.Random

class ClusterReceptionist extends Actor {

  // who is in the cluster an who is not?
  val cluster = Cluster(context.system)
  cluster.subscribe(self, classOf[ClusterEvent.MemberUp])
  cluster.subscribe(self, classOf[ClusterEvent.MemberRemoved])

  override def postStop(): Unit = {
    cluster.unsubscribe(self)
  }

  override def receive: Receive = awaitingMembers

  val awaitingMembers: Receive = {
    case current: ClusterEvent.CurrentClusterState =>
      // linearize
      val addresses = current.members.toVector map (_.address)
      // filter out the self address
      val notMe = addresses filter (_ != cluster.selfAddress)
      // change to the active node
      if (notMe.nonEmpty) context.become(active(notMe))

    // cluster monitoring: nodes can still be added or removed
    case MemberUp(member) if member.address != cluster.selfAddress =>
      context.become(active(Vector(member.address)))

    case Get(url) =>
      // no computing resources (no nodes available) at the moment
      sender ! Failed(url, "no nodes available")
  }

  def active(addresses: Vector[Address]): Receive = {
    // cluster monitoring: nodes can still be added or removed
    case MemberUp(member) if member.address != cluster.selfAddress =>
      context.become(active(addresses :+ member.address))

    case MemberRemoved(member, _) =>
      // filter member out from the addresses
      val next = addresses filterNot (_ == member.address)
      // if that was the last one
      if (next.isEmpty) context.become(awaitingMembers)
      else context.become(active(next))

      // using the information
    case Get(url) if context.children.size < addresses.size =>
      val client = sender
      val address = pick(addresses) // random pick
      context.actorOf(Props(new Customer(client, url, address))) // asynchronous execution, Props describes how to create an actor later

    case Get(url) =>
      sender ! Failed(url, "too many parallel queries")

  }

  def pick(v: Vector[Address]): Address = {
    Random.shuffle(v.toList).head
  }
}
