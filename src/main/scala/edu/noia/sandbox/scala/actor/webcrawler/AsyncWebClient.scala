package edu.noia.sandbox.scala.actor.webcrawler

import java.util.concurrent.Executor

import org.asynchttpclient.DefaultAsyncHttpClient

import scala.concurrent.{Future, Promise}
import scala.util.{Failure, Success, Try}

object AsyncWebClient {

  val client = new DefaultAsyncHttpClient

  def get(url: String)(implicit exec: Executor): Future[String] = {
    val p = Promise[String]()

    Try(client.prepareGet(url)) match {
      case Success(request) =>
        val f = request.execute()
        f.addListener(new Runnable {
          def run = {
            val response = f.get
            if (response.getStatusCode < 400)
              p.success(response.getResponseBody)
            else p.failure(BadStatus(response.getStatusCode))
          }
        }, exec)

      case Failure(_) => p.failure(BadStatus(400))
    }
    p.future
  }

  def shutdown = {
    client.close()
  }

  case class BadStatus(code: Int) extends Throwable {
    override def toString: String = code.toString
  }
}
