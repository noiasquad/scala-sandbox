package edu.noia.sandbox.scala.actor.counter

import akka.actor.{Actor, ActorLogging}

class Counter extends Actor with ActorLogging {

  def counter(n: Int): Receive = {
    case "inc" => {
      log.debug("{} inc by 1", n)
      context.become(counter(n + 1))
    }
    case "get" => {
      log.debug("get {}", n)
      sender ! n
    }
  }

  override def receive: Receive = counter(0)
}
