package edu.noia.sandbox.scala.actor.cluster

import akka.actor.Actor
import akka.cluster.{Cluster, ClusterEvent}
import edu.noia.sandbox.scala.actor.webcrawler.AsyncWebClient

/*
  ClusterWorker:
  user
  app

  remote
  akka.tcp://...  // ephemeral
  user/app/...  // ephemeral
  controller  // the logical child of the customer
  getter
 */
class ClusterWorker extends Actor {

  val cluster = Cluster(context.system)
  cluster.subscribe(self, classOf[ClusterEvent.MemberRemoved])

  val main = cluster.selfAddress.copy(port = Some(2552))
  cluster.join(main)

  override def receive: Receive = {
    case ClusterEvent.MemberRemoved(member, _) =>
      if (member.address == main) context.stop(self)
  }

  override def postStop(): Unit = {
    AsyncWebClient.shutdown
  }
}
