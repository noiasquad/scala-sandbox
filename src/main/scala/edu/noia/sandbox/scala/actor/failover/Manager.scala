package edu.noia.sandbox.scala.actor.failover

import akka.actor.{Actor, Props, Terminated}

/*
 Manager is supposed to connect to a database
*/
class Manager extends Actor {

  def prime(): Receive = {
    val db = context.actorOf(Props[DBActor], "db")
    context.watch(db)

    {
      case Terminated(_) => context.become(backup())
    }
  }

  /*
    connecting to a different database or
    storing updates in memory until the main database comes back online
   */
  def backup(): Receive = ???

  override def receive: Receive = prime()
}

class DBActor extends Actor {
  override def receive: Receive = ???
}