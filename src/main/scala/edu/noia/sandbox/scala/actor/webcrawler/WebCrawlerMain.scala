package edu.noia.sandbox.scala.actor.webcrawler

import akka.actor.{Actor, ActorLogging, Props, ReceiveTimeout}
import edu.noia.sandbox.scala.actor.webcrawler.Receptionist.{Failed, Result}

import scala.concurrent.duration._

class WebCrawlerMain extends Actor with ActorLogging {

  val receptionist = context.actorOf(Props[Receptionist], "receptionist")

  receptionist ! Get("http://www.google.com")
  receptionist ! Get("http://www.google.com/1")
  receptionist ! Get("http://www.google.com/2")
  receptionist ! Get("http://www.google.com/3")
  receptionist ! Get("http://www.google.com/4")

  context.setReceiveTimeout(10.seconds)

  override def receive: Receive = {
    case Result(url, links) =>
      println(links.toVector.sorted.mkString(s"Results for '$url':\n", "\n", "\n"))
    case Failed(url, reason) =>
      println(s"Failed to fetch '$url'\n")
    case ReceiveTimeout =>
      log.debug("timeout of {} reached", 10.seconds)
      context.stop(self)
  }

  override def postStop(): Unit = {
    AsyncWebClient.shutdown
  }

}
