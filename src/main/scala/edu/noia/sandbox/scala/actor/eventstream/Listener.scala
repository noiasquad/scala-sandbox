package edu.noia.sandbox.scala.actor.eventstream

import akka.actor.Actor

class Listener extends Actor {

  context.system.eventStream.subscribe(self, classOf[LogEvent])

  override def receive: Receive = {
    case e: LogEvent =>
  }

  override def postStop(): Unit = {
    context.system.eventStream.unsubscribe(self)
  }
}

case class LogEvent() {}
