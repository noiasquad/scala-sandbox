package edu.noia.sandbox.scala.actor.event

import akka.persistence.PersistentActor

class UserProcessorAsync extends PersistentActor {
  var state = State(Vector.empty, false)

  def receiveCommand = {
    case NewPost(text, id) =>
      if (!state.disabled) {
        val created = PostCreated(text)
        updateState(created)
        updateState(QuotaReached)
        persistAsync(created)(_ => sender() ! BlogPosted(id))
        persistAsync(QuotaReached)(_ => ())
      } else sender() ! BlogNotPosted(id, "quota reached")
  }

  def updateState(e: Event): Unit = {
    state = state.updated(e)
  }

  def receiveRecover = {
    case e: Event => updateState(e)
  }

  override def persistenceId: String = ???
}