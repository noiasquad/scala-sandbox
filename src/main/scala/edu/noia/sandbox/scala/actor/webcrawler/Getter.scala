package edu.noia.sandbox.scala.actor.webcrawler

import akka.Done
import akka.actor.{Actor, ActorLogging, Status}
import akka.pattern.pipe
import edu.noia.sandbox.scala.actor.webcrawler.Getter.Abort
import org.jsoup.Jsoup

import scala.collection.JavaConverters._

class Getter(url: String, depth: Int) extends Actor with ActorLogging {

  implicit val exec = context.dispatcher

  /*WebClient get url onComplete {
    case Success(body) => self ! body
    case Failure(err) => self ! Status.Failure(err)
  }*/

  log.debug("getting {} at depth {}", url, depth)

  AsyncWebClient get url pipeTo self

  override def receive: Receive = {
    case body: String =>
      for (link <- findLinks(body))
        context.parent ! Controller.Check(link, depth)
      stop()

    case Abort => stop()

    case s: Status.Failure =>
      log.debug("failure encountered {}", s.cause)
      stop()
  }

  def stop(): Unit = {
    context.parent ! Done
    context.stop(self)
  }

  def findLinks(body: String): Iterator[String] = {
    val document = Jsoup.parse(body, url)
    val links = document.select("a[href]")
    for {
      link <- links.iterator().asScala
    } yield link.attr("href")
  }

}
object Getter {

  case object Done

  case object Abort

}
