package edu.noia.sandbox.scala.actor.cluster

import akka.actor.{Actor, ActorRef, Address, Deploy, Props, ReceiveTimeout, SupervisorStrategy, Terminated}
import akka.remote.RemoteScope
import edu.noia.sandbox.scala.actor.webcrawler.{Controller, Receptionist}

import scala.concurrent.duration._

/*
  Responsible for making sure the given url is retrieved,
  but the work is supposed to be performed at a remote node which address is given
  It is an ephemeral actor
 */
class Customer(client: ActorRef, url: String, node: Address) extends Actor {

  implicit val s = context.parent // trick to modify the default sender passed when sending messages

  override val supervisorStrategy = SupervisorStrategy.stoppingStrategy
  val props = Props[Controller].withDeploy(Deploy(scope = RemoteScope(node)))
  val controller = context.actorOf(props, "controller")
  context.watch(controller)

  context.setReceiveTimeout(5.seconds)
  controller ! Controller.Check(url, 2) // this go over the network now

  // supervisation of the controller
  override def receive: Receive = ({
    case ReceiveTimeout =>
      context.unwatch(controller)
      client ! Receptionist.Failed(url, "controller timed out")

    case Terminated(_) =>
      client ! Receptionist.Failed(url, "controller died")

    case Controller.Result(links) =>
      context.unwatch(controller)
      client ! Receptionist.Result(url, links)
  }: Receive) andThen(_ => context.stop(self))
}
