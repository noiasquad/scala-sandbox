package edu.noia.sandbox.scala.actor.webcrawler

import akka.actor.{Actor, ActorLogging, ActorRef, OneForOneStrategy, Props, SupervisorStrategy, Terminated}
import akka.util.Timeout
import edu.noia.sandbox.scala.actor.webcrawler.Controller.{Check, Result}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

class DWController extends Actor with ActorLogging {

  context.system.scheduler.scheduleOnce(5.seconds, self, Timeout)

  override val supervisorStrategy = OneForOneStrategy(maxNrOfRetries = 5) {
    case _: Exception => SupervisorStrategy.Restart
  }

  var cache = Set.empty[String]

  override def receive: Receive = {
    case Check(url, depth) =>
      log.debug("{} checking {}", depth, url)

      if (!cache(url) && depth > 0)
        context.watch(context.actorOf(Props(new Getter(url, depth - 1))))
      cache += url

    case Terminated(_) =>
      if (context.children.isEmpty) context.parent ! Result(cache)

    case Timeout =>
      log.debug("timeout of {} reached", 5.seconds)
      context.children foreach context.stop
  }
}


