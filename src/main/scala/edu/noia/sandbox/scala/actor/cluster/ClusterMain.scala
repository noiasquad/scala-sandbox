package edu.noia.sandbox.scala.actor.cluster

import akka.actor.{Actor, Props, ReceiveTimeout}
import akka.cluster.{Cluster, ClusterEvent}
import edu.noia.sandbox.scala.actor.webcrawler.Get
import edu.noia.sandbox.scala.actor.webcrawler.Receptionist.{Failed, Result}

import scala.concurrent.duration._

/*
  ClusterMain:
  user
  app
  receptionist
  customer
 */
class ClusterMain extends Actor {

  val cluster = Cluster(context.system)
  cluster.subscribe(self, classOf[ClusterEvent.MemberUp])
  cluster.subscribe(self, classOf[ClusterEvent.MemberRemoved])
  cluster.join(cluster.selfAddress)

  val receptionist = context.actorOf(Props[ClusterReceptionist], "receptionist")
  context.watch(receptionist) // sign death pact

  def getLatter(d: FiniteDuration, url: String): Unit = {
    import context.dispatcher
    context.system.scheduler.scheduleOnce(d, receptionist, Get(url))
  }

  getLatter(Duration.Zero, "http://www.google.com")

  override def receive: Receive = {
    case ClusterEvent.MemberUp(member) =>
      if (member.address != cluster.selfAddress) { // then we know there is a ClusterWorker
        getLatter(1.seconds, "http://www.google.com")
        getLatter(2.seconds, "http://www.google.com/0")
        getLatter(2.seconds, "http://www.google.com/1")
        getLatter(3.seconds, "http://www.google.com/2")
        getLatter(4.seconds, "http://www.google.com/3")
        context.setReceiveTimeout(3.seconds)
      }

    case Result(url, links) =>
      println(links.toVector.sorted.mkString(s"Results for '$url':\n", "\n", "\n"))
    case Failed(url, reason) =>
      println(s"Failed to fetch '$url'\n")
    case ReceiveTimeout =>
      cluster.leave(cluster.selfAddress)
    case ClusterEvent.MemberRemoved(m, _) =>
      context.stop(self)
  }
}
