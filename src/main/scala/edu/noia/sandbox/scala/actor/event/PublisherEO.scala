package edu.noia.sandbox.scala.actor.event

import akka.persistence.PersistentActor

/*
  remember what it has done, not to do it again
 */
class PublisherEO extends PersistentActor {

  var expectedId = 0L

  override def receiveCommand: Receive = {
    case PublishPost(text, id) =>
      if (id > expectedId) () // it comes from the future, ignore it
      else if (id < expectedId) sender() ! PostPublished(id) // the confirmation must have been lost, confirm it again
      else persist(PostPublished(id)) { e =>
        sender() ! e
        expectedId += 1
      }
  }

  override def receiveRecover: Receive = {
    case PostPublished(id) => expectedId = id + 1
  }

  override def persistenceId: String = ???
}
