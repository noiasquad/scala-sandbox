package edu.noia.sandbox.scala.actor.event

import akka.actor.ActorPath
import akka.persistence.{AtLeastOnceDelivery, PersistentActor}

/*
  An Actor reference is coupled to the lifecycle of one actor incarnation, crash then restart, then ActorRef
  The name of the actor stays valid
 */
class UserProcessorALO(publisher: ActorPath) extends PersistentActor with AtLeastOnceDelivery {
  var state = State(Vector.empty, false)

  def receiveCommand = {
    case NewPost(text, id) =>
      persist(PostCreated(text)) { e =>
        deliver(publisher)(PublishPost(text, _))
        sender() ! BlogPosted(id)
      }
    case PostPublished(id) => confirmDelivery(id)
  }

  def updateState(e: Event): Unit = {
    state = state.updated(e)
  }

  def receiveRecover = {
    case PostCreated(text) => deliver(publisher)(PublishPost(text, _))
    case PostPublished(id) => confirmDelivery(id)
  }

  override def persistenceId: String = ???
}

case class PublishPost(text: String, id: Long) {}

case class PostPublished(id: Long) {}