package edu.noia.sandbox.scala.actor.counter

import akka.actor.{Actor, Props}

class CounterMain extends Actor {
  val counter = context.actorOf(Props[Counter], "counter")

  counter ! "inc"
  counter ! "inc"
  counter ! "inc"
  counter ! "get"

  override def receive: Receive = {
    case count: Int =>
      println(s"count was $count")
      context.stop(self)
  }
}
