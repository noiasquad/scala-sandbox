package edu.noia.sandbox.scala.actor.webcrawler

import akka.actor.{Actor, Props, SupervisorStrategy, Terminated}
import edu.noia.sandbox.scala.actor.webcrawler.Receptionist.{Failed, Job, Result}

/*
  Always stop the controller if it has a problem
  React to Terminated to catch cases where no Result was sent
  Discard Terminated after Result was sent
 */
class DWReceptionist extends Actor {

  // for any failure of any child issues a stop command
  override def supervisorStrategy = SupervisorStrategy.stoppingStrategy

  override def receive: Receive = waiting

  /*
    upon Get(url) start a traversal and become running
   */
  val waiting: Receive = {
    case Get(url) => context.become(runNext(Vector(Job(sender, url))))
  }

  /*
    upon Get(url) append that to queue and keep running
    upon Controller.Result(links) ship that to client and run next job from queue if any
   */
  def running(queue: Vector[Job]): Receive = {
    case Controller.Result(links) =>
      val job = queue.head
      job.client ! Result(job.url, links)
      context.stop(context.unwatch(sender))
      context.become(runNext(queue.tail))

    case Terminated(_) =>
      val job = queue.head
      job.client ! Failed(job.url)
      context.become(runNext(queue.tail))

    case Get(url) =>
      context.become(enqueueJob(queue, Job(sender, url)))
  }


  var requestNo = 0

  def runNext(queue: Vector[Job]): Receive = {
    requestNo += 1
    if (queue.isEmpty) waiting
    else {
      val controller = context.actorOf(Props[DWController], s"c$requestNo")
      context.watch(controller)
      controller ! Controller.Check(queue.head.url, 2)
      running(queue)
    }
  }

  def enqueueJob(queue: Vector[Job], job: Job): Receive = {
    if (queue.size > 3) {
      sender ! Failed(job.url)
      running(queue)
    } else running(queue :+ job)
  }

}


