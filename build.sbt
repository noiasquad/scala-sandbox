organization := "edu.noia.sandbox"
name := "scala-sandbox"
version := "0.1"

val akkaVersion = "2.5.15"

scalaVersion := "2.12.6"
libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % akkaVersion,
  "com.typesafe.akka" %% "akka-cluster" % akkaVersion,
  "com.typesafe.akka" %% "akka-persistence"% akkaVersion,
  "org.asynchttpclient" % "async-http-client" % "2.5.3",
  "org.jsoup" % "jsoup" % "1.8.1",
  "com.typesafe.akka" %% "akka-testkit" % akkaVersion % Test)
